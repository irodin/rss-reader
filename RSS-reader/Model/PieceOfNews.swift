//
//  PieceOfNews.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 27/01/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


protocol PieceOfNews {
	
	var title: String { get }
	
	var link: URL { get }
	
	var pubDate: Date { get }
	
	var description: String? { get }
	
}
