//
//  LentaNews.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 06/05/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


// Item of Lenta.ru RSS feed
struct LentaNews: PieceOfNews {
	
	let title: String
	
	let link: URL
	
	let pubDate: Date
	
	let description: String?
	
}


extension LentaNews: RSSParsable {

	static let endpoint: String = "https://www.gazeta.ru/export/rss/lenta.xml"

	static var recordKey: String { "item" }

	static var dictionaryKeys: Set<String> { Set<String>(["title", "link", "pubDate", "description"])
	}

	static func parser(data: Data) -> RSSParserProtocol {
		return RSSParser<LentaNews>(data: data)
	}

	init?(json: [String: String]) {
		guard let title = json["title"],
			let link = URL(string: json["link"]!),
			let pubDateString = json["pubDate"] else {
			return nil
		}

		self.title = title
		self.link = link
		self.description = json["description"]

		let dateFormatter = DateFormatter()
		dateFormatter.locale = Locale(identifier: "en_US_POSIX")
		dateFormatter.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
		guard let pubDate = dateFormatter.date(from: pubDateString) else {
			return nil
		}
		self.pubDate = pubDate
	}
}
