//
//  RSSError.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 28/04/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


enum RSSError: Error {
	
    case invalidURL
    case requestFailed
    case invalidData
    case responseUnsuccessful
    case xmlParsingFailure
    case xmlConversionFailure

    var localizedDescription: String {
        switch self {
		case .invalidURL: return "Invalid URL"
        case .requestFailed: return "Request Failed"
        case .invalidData: return "Invalid Data"
        case .responseUnsuccessful: return "Response Unsuccessful"
        case .xmlParsingFailure: return "XML Parsing Failure"
        case .xmlConversionFailure: return "XML Conversion Failure"
        }
    }

}
