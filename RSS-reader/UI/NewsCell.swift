//
//  NewsCell.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 04/02/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {
	
	@IBOutlet weak var titleLabel: UILabel!
	@IBOutlet weak var pubDateLabel: UILabel!
	@IBOutlet weak var descriptionLabel: UILabel!

	func update(withNews aNews: PieceOfNews) {
		titleLabel.text = aNews.title
		descriptionLabel.text = aNews.description
		
		pubDateLabel.text = pubDateString(aNews)
	}
	
	func pubDateString(_ aNews: PieceOfNews) -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.locale = .current
		dateFormatter.dateFormat = "MMM d, HH:mm"
		return dateFormatter.string(from: aNews.pubDate)
	}

}
