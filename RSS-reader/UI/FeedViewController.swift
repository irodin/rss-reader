//
//  ViewController.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 27/01/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import UIKit

class FeedViewController: UITableViewController {
	
	let service = RSSService()
	
	private var news = [PieceOfNews]()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		let refreshControl = UIRefreshControl()
		refreshControl.addTarget(self, action:  #selector(updateNews), for: .valueChanged)
		self.refreshControl = refreshControl
		
		self.updateNews()
	}
	
	@objc func updateNews() {
		service.get { [weak self] (result) in
			switch result {
			case .success(let news):
				self?.news = news
			case .failure(let error):
				print(error)
			}
			
			DispatchQueue.main.async {
				self?.refreshControl?.endRefreshing()
				self?.tableView.reloadData()
			}
		}
	}

}

extension FeedViewController/*: UITableViewDataSource*/ {
	
	override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.news.count
	}

	override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "newsCellId", for: indexPath) as! NewsCell
		
		let aNews = self.news[indexPath.row]
		cell.update(withNews: aNews)
		
		return cell
	}
	
}

