//
//  RSSParser.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 07/02/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


protocol RSSParserProtocol {
	
	typealias RSSParserCompletionHandler = (Result<[[String: String]], RSSError>) -> Void
	
	init(data: Data)
	
	func parse(completionHandler completion: @escaping RSSParserCompletionHandler)
}


class RSSParser<T: RSSParsable>: NSObject, XMLParserDelegate, RSSParserProtocol {
	
	let parser: XMLParser

	var currentDictionary = [String: String]()
	var currentKeyValue: String?
	
	var parsedData = [[String: String]]()
	var error: Error?
	
	/// RSSParserProtocol
	
	required init(data: Data) {
		self.parser = XMLParser(data: data)
		super.init()
		self.parser.delegate = self
	}
	
	func parse(completionHandler completion: @escaping RSSParserCompletionHandler) {
		if parser.parse() {
			completion(.success(parsedData))
		} else {
			completion(.failure(.xmlParsingFailure))
		}
	}
	
	/// XMLParserDelegate
		
	func parserDidStartDocument(_ parser: XMLParser) {
		parsedData = []
	}

	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
		if elementName == T.recordKey {
			currentDictionary = [:]
		} else if T.dictionaryKeys.contains(elementName) {
			currentKeyValue = ""
		}
	}

	func parser(_ parser: XMLParser, foundCharacters string: String) {
		currentKeyValue? += string
	}

	func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
		if elementName == T.recordKey {
			parsedData.append(currentDictionary)
			currentDictionary = [:]
		} else if T.dictionaryKeys.contains(elementName) {
			currentDictionary[elementName] = currentKeyValue
			currentKeyValue = nil
		}
	}

	func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
		self.error = parseError
	}
	
}
