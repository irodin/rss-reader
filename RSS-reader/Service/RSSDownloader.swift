//
//  RSSDownloader.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 29/01/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


class RSSDownloader: NSObject {
	
    typealias RSSDownloadCompletionHandler = (Result<Data, RSSError>) -> Void
	
	lazy var session = URLSession(configuration: .default, delegate: self, delegateQueue: nil)
	
	func download(url: URL, completionHandler completion: @escaping RSSDownloadCompletionHandler) {
		let task = session.dataTask(with: url) { data, response, error in
			
			if error != nil {
				completion(.failure(.requestFailed))
				return
			}
			
			guard let httpResponse = response as? HTTPURLResponse, (200...299).contains(httpResponse.statusCode) else {
				completion(.failure(.responseUnsuccessful))
				return
			}
			
			guard let data = data else {
				completion(.failure(.invalidData))
				return
			}
			
			completion(.success(data))
		}
		task.resume()
	}

}

extension RSSDownloader: URLSessionDelegate {
	
	func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
		let urlCredential = URLCredential(trust: challenge.protectionSpace.serverTrust!)
		completionHandler(.useCredential, urlCredential)
	}
	
}
