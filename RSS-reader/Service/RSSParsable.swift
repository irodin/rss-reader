//
//  RSSParsable.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 13/02/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation

protocol RSSParsable {
	
	static var endpoint: String { get }
	
	static var recordKey: String { get }
	
	static var dictionaryKeys: Set<String> { get }
	
	static func parser(data: Data) -> RSSParserProtocol
	
	init?(json: [String: String])
	
}
