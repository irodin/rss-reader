//
//  RSSService.swift
//  RSS-reader
//
//  Created by Ilya Rodin on 28/04/2020.
//  Copyright © 2020 Ilya Rodin. All rights reserved.
//

import Foundation


protocol Gettable {
    associatedtype T
    func get(completion: @escaping (Result<T, RSSError>) -> Void)
}


struct RSSService: Gettable {
	
    let downloader = RSSDownloader()

    typealias RSSCompletionHandler = (Result<[LentaNews], RSSError>) -> ()

    func get(completion: @escaping RSSCompletionHandler) {
        
        guard let url = URL(string: LentaNews.endpoint) else {
            completion(.failure(.invalidURL))
            return
        }
      
		downloader.download(url: url) { downloadResult in
			switch downloadResult {
			case .failure(let error):
				completion(.failure(error))
				return
			case .success(let data):
				let parser = RSSParser<LentaNews>(data: data)
				parser.parse { parseResult in
					switch parseResult {
					case .failure(let error):
						completion(.failure(error))
						return
					case .success(let parsedData):
						var news = [LentaNews]()
						for item in parsedData {
							if let pieceOfNews = LentaNews.init(json: item) {
								news.append(pieceOfNews)
							} else {
								completion(.failure(.xmlConversionFailure))
							}
						}
						completion(.success(news))
					}
				}
			}
		}
	}
}
